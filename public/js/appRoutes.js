angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })
        .when('/product', {
            templateUrl: 'views/product.html',
            controller: 'ProductController'
        })

    $locationProvider.html5Mode(true);

}]);